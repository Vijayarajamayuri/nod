/** 
*  Account model
*  Describes the characteristics of each attribute in a account resource.
*
* @author Chitralekha Chikku<S534630@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const AccountSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  balance: { type: Number, required: true, unique: false},
  branch: { type: String, required: true}
  })

module.exports = mongoose.model('Account', AccountSchema)