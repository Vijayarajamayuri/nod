/** 
*  Category model
*  Describes the characteristics of each attribute in a customer resource.
*
* @author Vijaya Raja Mayuri Akula <S534685@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const CategorySchema= new mongoose.Schema({
  _id: { type: Number, required: true },
  category_type: { type: String, required: true }
  
})

module.exports = mongoose.model('Category', CategorySchema)