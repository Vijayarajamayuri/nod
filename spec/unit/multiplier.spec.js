var multiplier = require('../../services/multiplier');

describe("multiplier tests ", function() {
    var _numberA;
    var _numberB;

    it("should return numberA multiply with numberB", function() {
        _numberA = 6;
        _numberB = 2;
        var result = multiplier.multiply(_numberA, _numberB);

        expect(result).toEqual(12);
    });
    it("should return 0 for numberB equals 0", function() {
        _numberA = 2;
        _numberB = 0;
        var result = multiplier.multiply(_numberA, _numberB);

        expect(result).toEqual(0);
    });

});